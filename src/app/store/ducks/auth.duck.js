import { createMigrate, persistReducer } from "redux-persist" // localStorage
import { call, put, takeLatest } from "redux-saga/effects"
import storage from "redux-persist/lib/storage"
import * as routerHelpers from "../../router/RouterHelpers"
import { login } from "../../crud/auth.crud"
import { register } from "../../crud/auth.crud"
import { logout } from "../../crud/auth.crud"
import { requestPassword } from "../../crud/auth.crud"
//import { REHYDRATE } from 'redux-persist'

export const actionTypes = {
  LOGIN: "[Login] Action",
  LOGIN_SUCCESS : "[Login Success] Action",
  LOGIN_FAIL : "[Login Fail] Action",
  LOGOUT: "[Logout] Action",
  REGISTER: "[Register] Action",
  REGISTER_SUCCESS : "[Register Success] Action",
  REGISTER_FAIL : "[Register Fail] Action",
  REQUEST_PASSWORD: "[Request Password] Action",
  REQUEST_PASSWORD_SUCCESS: '[Request Password Success] Action"',
  REQUEST_PASSWORD_FAIL: '[Request Password Fail] Action"'
};

// Original
// const initialAuthState = {
//   user: undefined,
//   authToken: undefined
// };

const iniState = {
  user: {
    id: undefined,
    username: undefined,
    fullname: undefined,
    email: undefined,
    pic: undefined
  },
  authToken: undefined,
  isAuthorized: false,
  error: undefined
}

const migrations = { 
  2: (state) => {
    return {
      ...state,
      user: {
        id: undefined,
        username: undefined,
        fullname: undefined,
        email: undefined,
        pic: undefined
      },
      authToken: undefined,
      isAuthorized: false,
      error: undefined
    }
  }
} 

const persistConfig = {
  storage, 
  version: 2, 
  key: "demo1-auth", 
  whitelist: ['user', 'authToken', 'isAuthorized'], 
  migrate: createMigrate(migrations, { debug: true })
}

export const reducer = persistReducer(
    // { storage, key: "demo1-auth", whitelist: ['user', 'authToken', 'isAuthorized']/*["user", "authToken"]*/ },
    persistConfig,
    (state = iniState, action) => {
      switch (action.type) {
        // case actionTypes.LOGIN: {
        //   return { ...state, isAuthorized: true }
        //   // return { ...state, error: "" }
        // }
        
        case actionTypes.LOGIN_SUCCESS: {
          return { ...state, user: action.payload.data.user, authToken: action.payload.authToken, isAuthorized: true }
        }

        case actionTypes.LOGIN_FAIL: {
          return { ...state, error: action.payload.error}
        }

        case actionTypes.REGISTER_SUCCESS: {
          return { ...state, user: action.payload.data.user, authToken: action.payload.authToken, isAuthorized: true }
        }

        case actionTypes.REGISTER_FAIL: {
          return { ...state, error: action.payload.error }
        }

        case actionTypes.LOGOUT: {
          routerHelpers.forgotLastLocation();
          logout(action.payload.id)
          return iniState;
        }
        
        case actionTypes.REQUEST_PASSWORD_SUCCESS: {
          return { ...state, user: action.payload.data }
        }

        case actionTypes.REQUEST_PASSWORD_FAIL: {
          return { ...state, error: action.payload.error}
        }

        default:
          return state;
      }
    }
);

export const actions = {
  login: (email, password) => ({
    type: actionTypes.LOGIN,
    payload: { email: email, password: password }
  }),
        
  doLoginSuccess: (data) => ({
    type: actionTypes.LOGIN_SUCCESS,
    payload: { data: data }
  }),

  doLoginFail: (error) => ({
    type: actionTypes.LOGIN_FAIL,
    payload: { error: error }
  }),

  register: (username, fullname, email, password) => ({
    type: actionTypes.REGISTER,
    payload: { username: username, fullname: fullname, email: email, password: password }
  }),

  doRegisterSuccess: (data) => ({
    type: actionTypes.REGISTER_SUCCESS,
    payload: { data: data }
  }),

  doRegisterFail: (error) => ({
    type: actionTypes.REGISTER_FAIL,
    payload: { error: error }
  }),

  logout: (id) => ({ 
    type: actionTypes.LOGOUT,
    payload: { id: id }
  }),

  requestPassword: (email) => ({
    type: actionTypes.REQUEST_PASSWORD,
    payload: { email: email }
  }),

  doRequestPasswordSuccess: (data) => ({
    type: actionTypes.REQUEST_PASSWORD_SUCCESS,
    payload: { data: data }
  }),

  doRequestPasswordFail: (error) => ({
    type: actionTypes.REQUEST_PASSWORD_FAIL,
    payload: { error: error }
  })
};

export function* saga() {
  yield takeLatest( actionTypes.LOGIN, function* loginSaga(action) {
    try {
      const response = yield call (login, action.payload) 
      const data = {
        user: {
          id: response.data.user.id,
          username: response.data.user.username,
          fullname: response.data.user.fullname,
          email: response.data.user.email,
          pic: response.data.user.pic
        },
        authToken: response.data.authToken
      }
      console.log("Data in takelatest login in auth.duck: ", data)
      yield put(actions.doLoginSuccess(data))
    }
    catch (error) {
      console.log("Login axios post error is!: ", error)
      yield put(actions.doLoginFail(error))
    }
  });

  yield takeLatest(actionTypes.REGISTER, function* registerSaga(action) {
    console.log("Payload in register takelatest, ", action.payload)
    try {
      const response = yield call (register, action.payload)
      const data = {
        user: {
          id: response.data.user.id,
          username: response.data.user.username,
          fullname: response.data.user.fullname,
          email: response.data.user.email,
          pic: response.data.user.pic
        },
        authToken: response.data.authToken
      }
      console.log("Data in takelatest register in auth.duck: ", data)
      yield put(actions.doRegisterSuccess(data))
    }
    catch (error) {
      console.log("Register axios post error.message is!: ", error.response)
      const errorData = {
        status: error.response.status,
        statusText: error.response.statusText,
        message: error.response.data
      }
      yield put(actions.doRegisterFail(errorData))
    }
  });

  yield takeLatest( actionTypes.REQUEST_PASSWORD, function* requestPasswordSaga(action) {
    try {
      const response = yield call (requestPassword, action.payload) 
      const data =  {
        id: response.data.id,
        email: response.data.email,
        password: response.data.password
      }

      console.log("Data in takelatest requestPassword in auth.duck: ", data)
      yield put(actions.doRequestPasswordSuccess(data))
    }
    catch (error) {
      console.log("requestPassword axios post error is!: ", error)
      yield put(actions.doRequestPasswordFail(error))
    }
  });
}