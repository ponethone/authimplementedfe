import axios from "axios"

export const LOGIN_URL = "http://localhost:3001/auth/login"
export const REGISTER_URL = "http://localhost:3001/auth/register"
export const LOGOUT_URL = "http://localhost:3001/auth/logout/"
export const REQUEST_PASSWORD_URL = "http://localhost:3001/auth/forgotPassword/"

export function login(payload) {
  return axios.post(LOGIN_URL, payload);
}

export function register(payload) {
  return axios.post(REGISTER_URL, payload);
}

export function logout(id) {
  return axios.post(LOGOUT_URL + id);
  //localStorage.removeItem("user");
}

export function requestPassword(email) {
  return axios.post(REQUEST_PASSWORD_URL + email);
}