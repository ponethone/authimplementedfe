import React, { useState } from "react"
import { useHistory, Link } from "react-router-dom"
import { Formik } from "formik"
import { connect } from "react-redux"
import { FormattedMessage, injectIntl } from "react-intl"
import { TextField } from "@material-ui/core"
import clsx from "clsx"
import * as auth from "../../store/ducks/auth.duck"
//import { login } from "../../crud/auth.crud";"

function Login(props) {
  // if (props.authData)
  //   //history.push('/party')
  //   console.log("PropsUser", props.authData)

  const history = useHistory()
  const { intl } = props
  const [loading, setLoading] = useState(false)
  const [loadingButtonStyle, setLoadingButtonStyle] = useState({
    paddingRight: "2.5rem"
  });

  const enableLoading = () => {
    setLoading(true)
    setLoadingButtonStyle({ paddingRight: "3.5rem" })
  }

  const disableLoading = () => {
    setLoading(false)
    setLoadingButtonStyle({ paddingRight: "2.5rem" })
  }

  return (
    <>
      {/* <div className="kt-login__head">
        <span className="kt-login__signup-label">
          Don't have an account yet?
        </span>
        &nbsp;&nbsp;
        <Link to="/auth/registration" className="kt-link kt-login__signup-link">
        // <Link to="/auth/register" className="kt-link kt-login__signup-link">
          Sign Up!
        </Link>
      </div> */}

      <div className="kt-login__body">
        <div className="kt-login__form">
          <div className="kt-login__title">
            <h3>
              <FormattedMessage id="AUTH.LOGIN.TITLE" />
            </h3>
          </div>

          <Formik
            initialValues={{
              email: "",//"admin@demo.com",
              password: ""//</div></div>"demo"
            }}
            validate={values => {
              const errors = {}

              if (!values.email) 
              {
                // https://github.com/formatjs/react-intl/blob/master/docs/API.md#injection-api
                errors.email = intl.formatMessage({
                  id: "AUTH.VALIDATION.REQUIRED_FIELD"
                })
              } 
              // else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) 
              // else if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/i.test(values.email)) 
              else if (!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(values.email)) 
              {
                errors.email = intl.formatMessage({
                  id: "AUTH.VALIDATION.INVALID_FIELD"
                })
              }

              if (!values.password) {
                errors.password = intl.formatMessage({
                  id: "AUTH.VALIDATION.REQUIRED_FIELD"
                })
              }
              return errors
            }}

            onSubmit={(values, { setStatus, setSubmitting }) => {
              console.log("Entered values in login form: ", values)

              enableLoading()

              // async function check() {
              //   try {
              //     await props.login(values.email, values.password)
              //     return props.authData.isAuthorized;
              //   } 
              //   catch(err){
              //     console.log(err);
              //   }
              // }
              // check()
              // // .then((res) => console.log(res))
              // .then((message) => {
              //   if(message) {
              //     disableLoading()
              //     // props.login(accessToken)
              //     history.push('/party')
              //   }
              //   else {
              //     disableLoading()
              //     setSubmitting(false)
              //     setStatus(
              //       intl.formatMessage({
              //         id: "AUTH.VALIDATION.INVALID_LOGIN"
              //       })
              //     )
              //   }
              // });


              // OK OK OK
              // async function check() {
              //   try {
              //     await props.login(values.email, values.password)
              //     return props.authData.isAuthorized;
              //   } 
              //   catch(err){
              //     console.log(err);
              //   }
              // }
              // check()
              // // .then((res) => console.log(res))
              // .then((message) => {
              //   if(message) {
              //     disableLoading()
              //     // props.login(accessToken)
              //     history.push('/party')
              //   }
              //   // else {
              //   //   disableLoading()
              //   //   setSubmitting(false)
              //   //   setStatus(
              //   //     intl.formatMessage({
              //   //       id: "AUTH.VALIDATION.INVALID_LOGIN"
              //   //     })
              //   //   )
              //   // }
              // })
              // .catch((err) => {
              //   disableLoading()
              //     setSubmitting(false)
              //     setStatus(
              //       intl.formatMessage({
              //         id: "AUTH.VALIDATION.INVALID_LOGIN"
              //       })
              //     )
              // });


              // 10:02p.m.
              // async function check() {
              //   console.log("Inside check() outside before try{}")
              //   try {
              //     await props.login(values.email, values.password)
              //     // setTimeout(async () => {
              //     //   console.log("In async check props.authData.isAuthorized: " + props.authData.isAuthorized)
              //     //   return props.authData.isAuthorized;
              //     // }, 50000)
              //     console.log("Inside check() and try{}")
              //     console.log("props.authData.isAuthorized " + props.authData.isAuthorized)
              //     return props.authData.isAuthorized;
              //   } 
              //   catch(err){
              //     console.log(err);
              //   }
              // }
              // check()
              // .then((message) => {
              //   console.log(".then " + props.authData.isAuthorized)
              //   if(!message) {
              //     disableLoading()
              //     setSubmitting(false)
              //     setStatus(
              //       intl.formatMessage({
              //         id: "AUTH.VALIDATION.INVALID_LOGIN"
              //       })
              //     )
              //   }
              //   // if(message === true) {
              //   //   disableLoading()
              //   //   // props.login(accessToken)
              //   //   history.push('/party')
              //   // }
              //   // else if(message === false) {
              //   //   disableLoading()
              //   //   setSubmitting(false)
              //   //   setStatus(
              //   //     intl.formatMessage({
              //   //       id: "AUTH.VALIDATION.INVALID_LOGIN"
              //   //     })
              //   //   )
              //   // }
              // })
              // .catch((err) => {
              //   console.log("In Catch")
              //   disableLoading()
              //   setSubmitting(false)
              //   setStatus(
              //     intl.formatMessage({
              //       id: "AUTH.VALIDATION.INVALID_LOGIN"
              //     })
              //   )
              // })



              // async function loginFunc() { 
              //   await props.login(values.email, values.password)
              // }
              // async function check() { 
              //   await loginFunc()
              //   .then(() => {
              //     if(props.authData.isAuthorized) {
              //       disableLoading()
              //       // props.login(accessToken)
              //       history.push('/party')
              //     }
              //     else {
              //       disableLoading()
              //       setSubmitting(false)
              //       setStatus(
              //         intl.formatMessage({
              //           id: "AUTH.VALIDATION.INVALID_LOGIN"
              //         })
              //       )
              //     }
              //   })
              // }
              // check();

              setTimeout(async () => {
                console.log("In setTimeout")
                await props.login(values.email, values.password)
                if(props.authData.isAuthorized) {
                  disableLoading()
                  // props.login(accessToken)
                  // history.push('/party')
                  history.push('/homepage')


                }
                else {
                  disableLoading()
                  setSubmitting(false)
                  setStatus(
                    intl.formatMessage({
                      id: "AUTH.VALIDATION.INVALID_LOGIN"
                    })
                  )
                }
              }, 1000)


              // setTimeout(async () => {
              //   await props.login(values.email, values.password)
              //   console.log("In Login.js setTimeout")
              //   console.log("props.authData.isAuthorized:" + props.authData.isAuthorized)
              //   // if(props.authData.isAuthorized) {
              //   if(props.authData.isAuthorized) {
              //     console.log("In Login.js isAuthorized true")
              //     disableLoading()
              //     // props.login(accessToken)
              //     history.push('/party')
              //   }
              //   else if(!props.authData.isAuthorized) {
              //     console.log("In Login.js isAuthorized false")
              //     disableLoading()
              //     setSubmitting(false)
              //     setStatus(
              //       intl.formatMessage({
              //         id: "AUTH.VALIDATION.INVALID_LOGIN"
              //       })
              //     )
              //   }
              // }, 1000)
            }}



            // setTimeout(async () => {
            //   await props.login(values.email, values.password)
            //   if(props.authData.isAuthorized) {
            //     disableLoading()
            //     // props.login(accessToken)
            //     history.push('/party')
            //   }
            //   else {
            //     disableLoading()
            //     setSubmitting(false)
            //     setStatus(
            //       intl.formatMessage({
            //         id: "AUTH.VALIDATION.INVALID_LOGIN"
            //       })
            //     )
            //   }
            // }, 1000)
              


            // Fine 
            // onSubmit={(values, { setStatus, setSubmitting }) => {
            //   console.log("Entered values in login form: ", values)
            //   enableLoading();
            //   setTimeout(() => {
            //     props.login(values.email, values.password)
            //     if(props.authData.isAuthorized) { //if(props.authData.authToken) { //if (props.user.authToken) {
            //       disableLoading();
            //       // props.login(accessToken);
            //       history.push('/party')
            //     }
            //     else {
            //       disableLoading();
            //       setSubmitting(false);
            //       setStatus(
            //         intl.formatMessage({
            //           id: "AUTH.VALIDATION.INVALID_LOGIN"
            //         })
            //       );
            //     }
            //   }, 1000);
            // }}
          >
            {({
              values,
              status,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting
            }) => (
              <form
                noValidate={true}
                autoComplete="off"
                className="kt-form"
                onSubmit={handleSubmit}
              >
                {status ? (
                  <div role="alert" className="alert alert-danger">
                    <div className="alert-text">{status}</div>
                  </div>
                ) : (
                  <></>
                  // <div role="alert" className="alert alert-info">
                  //   <div className="alert-text">
                  //     Use account <strong>admin@demo.com</strong> and password{" "}
                  //     <strong>demo</strong> to continue.
                  //   </div>
                  // </div>
                )}

                <div className="form-group">
                  <TextField
                    type="email"
                    label="Email"
                    margin="normal"
                    className="kt-width-full"
                    name="email"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.email}
                    helperText={touched.email && errors.email}
                    error={Boolean(touched.email && errors.email)}
                  />
                </div>

                <div className="form-group">
                  <TextField
                    type="password"
                    margin="normal"
                    label="Password"
                    className="kt-width-full"
                    name="password"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.password}
                    helperText={touched.password && errors.password}
                    error={Boolean(touched.password && errors.password)}
                  />
                </div>

                <div className="kt-login__actions">
                  <Link
                    // to="/auth/registration"
                    to="/auth/register"
                    className="kt-link kt-login__link-forgot"
                  >
                    Don't have an account yet?
                    {/* <FormattedMessage id="AUTH.GENERAL.FORGOT_BUTTON" /> */}
                  </Link>

                  <button
                    id="kt_login_signin_submit"
                    type="submit"
                    disabled={isSubmitting}
                    className={`btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                      {
                        "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                      }
                    )}`}
                    style={loadingButtonStyle}
                  >
                    Login
                  </button>
                </div>
              </form>
            )}
          </Formik>

          {/* <div className="kt-login__divider">
            <div className="kt-divider">
              <span />
              <span>OR</span>
              <span />
            </div>
          </div> */}

          {/* <div className="kt-login__options">
            <Link to="http://facebook.com" className="btn btn-primary kt-btn">
              <i className="fab fa-facebook-f" />
              Facebook
            </Link>
            <Link to="http://twitter.com" className="btn btn-info kt-btn">
              <i className="fab fa-twitter" />
              Twitter
            </Link>
            <Link to="google.com" className="btn btn-danger kt-btn">
              <i className="fab fa-google" />
              Google
            </Link>
          </div> */}
        </div>
      </div>
    </>
  );
}

const stateToProps = state => {
  return {
    //user: state
    authData: state.auth
  }
}

export default injectIntl(
  connect(
    stateToProps,
    auth.actions
  )(Login)
)
