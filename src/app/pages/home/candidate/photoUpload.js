// Saves Image well
import React, { useEffect, useState } from 'react'
import { useDropzone } from 'react-dropzone'
import /*create,*/ {formData} from './create'
//import Image from 'react-image-resizer'
// import formData from './create'
//import create, {formData} from './create'

const thumbsContainer = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 16
};

const thumb = {
    display: 'inline-flex',
    borderRadius: 2,
    border: '1px solid #eaeaea',
    marginBottom: 8,
    marginRight: 8,
    // width: 125,//180,
    // height: 120,
    maxWidth: "100%",
    maxHeight:"100%",
    padding: 4,
    boxSizing: 'border-box'
};

const dropzone = {
    width: "80%",
    height: "30%",
    border: "1px black",
    borderStyle: 'inset'
}

const thumbInner = {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden'
};

const img = {
    display: 'block',
    width: 'auto',
    height: '100%'
};

export default function Previews(props) {
    //const { formData, setFormData } = useContext(ReferenceDataContext);
    const [files, setFiles] = useState([]);
    const { getRootProps, getInputProps } = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => {
            setFiles(acceptedFiles.map(file => Object.assign(file, {
                preview: URL.createObjectURL(file)
            })));
            for (let i = 0; i < acceptedFiles.length; i++) {
                // formData.append('image_BLOB', acceptedFiles[i]);
                formData.append('image_BLOB', acceptedFiles[i]); //.set()
            } 
        }
    });
    console.log("FormData in photoUpload:", formData)

    const thumbs = files.map(file => (
        <div style={thumb} key={file.name}>
            <div style={thumbInner}>
                <img    
                    alt=""
                    src={file.preview}
                    style={img}
                />
            </div>
        </div>
    ));

    useEffect(() => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
    }, [files]);

    // const ref = React.createRef();
    // useEffect(() => {
	// 	async function fetchData() {
	// 		const response = await axios.get(
	// 			'http://localhost:3001/candidates',
	// 		);
	// 		setFiles(response.data);
	// 	}
	// 	fetchData();
	// }, []);

    return (
        <>
            {
                files.length !== 0 ? (
                    <div style={thumbsContainer}>
                        {thumbs}
                    </div>
                ) :

                <div {...getRootProps()} style={dropzone} >
                    <input {...getInputProps()} />
                    <p><center>Upload</center></p>
                    {/* <Image src='https://getstamped.co.uk/wp-content/uploads/WebsiteAssets/Placeholder.jpg' width={50} height={50}/> */}
                    {/* <Image src='https://getstamped.co.uk/wp-content/uploads/WebsiteAssets/Placeholder.jpg' width={150} height={150} /> */} 
                    {/* <Image src='https://getstamped.co.uk/wp-content/uploads/WebsiteAssets/Placeholder.jpg' width={150} height={112} /> */}
                    {/* <Image src='https://getstamped.co.uk/wp-content/uploads/WebsiteAssets/Placeholder.jpg' height={112} /> */}
                    <i></i>
                </div>
            }
        </>
    );
}


// import React, { useEffect, useState, useContext } from 'react';
// import { useDropzone } from 'react-dropzone';
// import axios from 'axios';
// import { ReferenceDataContext } from "../../../../ReferenceDataContext"

// const thumbsContainer = {
//     display: 'flex',
//     flexDirection: 'row',
//     flexWrap: 'wrap',
//     marginTop: 16
// };

// const thumb = {
//     display: 'inline-flex',
//     borderRadius: 2,
//     border: '1px solid #eaeaea',
//     marginBottom: 8,
//     marginRight: 8,
//     width: 180,
//     height: 120,
//     padding: 4,
//     boxSizing: 'border-box'
// };

// const thumbInner = {
//     display: 'flex',
//     minWidth: 0,
//     overflow: 'hidden'
// };

// const img = {
//     display: 'block',
//     width: 'auto',
//     height: '100%'
// };


// export default function Previews(props) {
//     const { formData, setFormData } = useContext(ReferenceDataContext);
//     const [files, setFiles] = useState([]);
//     const { getRootProps, getInputProps } = useDropzone({
//         accept: 'image/*',
//         onDrop: acceptedFiles => {
//             setFiles(acceptedFiles.map(file => Object.assign(file, {
//                 preview: URL.createObjectURL(file)
//             })));
            
//             // const fileObjects = acceptedFiles.map(file => {
//             //     console.log(file)
//             //     // formData.append('assets', file, file.name)
//             //     setFormData({...formData, [file]: file.name});
//             // })
//             const assignFormData = acceptedFiles.map(file => {
//                 console.log(file)
//                 // formData.append('assets', file, file.name)
//                 setFormData({...formData, [file]: file.name});
//             })
//             assignFormData()
//             console.log(formData)

//             // Mine
//             // const formData = new FormData();
// 			// for (const [key, value] of Object.entries(acceptedFiles)) {
// 			// 	formData.append(key, value)
//             // }
//             // for (var i = 0; i < acceptedFiles.length; i++) {
//             //     formData.append('articleFiles[]', acceptedFiles[i]);
//             // }  
//             for (const [key, value] of Object.entries(acceptedFiles)) {
//                 ///setFormData(formData.concact(formData.append(key, value)))
//                 // setFormData(...formData, [key, value])
//                 //setFormData(...formData, [key]: value)
//                 setFormData({...formData, [key]: value});
//             }
//         }
//     });

//     const thumbs = files.map(file => (
//         <div style={thumb} key={file.name}>
//             <div style={thumbInner}>
//                 <img
//                     src={file.preview}
//                     style={img}
//                 />
//             </div>
//         </div>
//     ));

//     useEffect(() => () => {
//         // Make sure to revoke the data uris to avoid memory leaks
//         files.forEach(file => URL.revokeObjectURL(file.preview));
//     }, [files]);

//     // const ref = React.createRef();
//     // useEffect(() => {
// 	// 	async function fetchData() {
// 	// 		const response = await axios.get(
// 	// 			'http://localhost:3001/candidates',
// 	// 		);
// 	// 		setFiles(response.data);
// 	// 	}
// 	// 	fetchData();
// 	// }, []);

//     return (
//         <>
//             {
//                 files.length !== 0 ? (
//                     <div style={thumbsContainer}>
//                         {thumbs}
//                     </div>
//                 ) :

//                 <div {...getRootProps()} style={{
//                     width: "80%",
//                     height: "30%",
//                     border: "1px solid black",
//                     borderStyle: 'dotted'
//                 }} >
//                     <input {...getInputProps()} />
//                     <p>Upload</p>
//                 </div>
//             }
//         </>
//     );
// }

// import React, { useEffect, useState, useContext } from 'react'
// import { /*shallowEqual, useSelector,*/ useDispatch, connect } from "react-redux";
// import { useDropzone } from 'react-dropzone'
// import axios from 'axios'
// import { ReferenceDataContext } from "../../../../ReferenceDataContext"
// // import formData from './create'
// //import create, {formData} from './create'
// import * as candi from "../../../store/ducks/candidateForm.duck"

// // const {candidate} = useSelector(
// //     ({candidateForm}) => ({
// //         candidateForm
// //     }), 
// //     shallowEqual
// // )
// // const {candidate} = useSelector(
// //     (state) => {
// //         state.candidateForm
// // })

// const dispatch = useDispatch()

// const thumbsContainer = {
//     display: 'flex',
//     flexDirection: 'row',
//     flexWrap: 'wrap',
//     marginTop: 16
// };

// const thumb = {
//     display: 'inline-flex',
//     borderRadius: 2,
//     border: '1px solid #eaeaea',
//     marginBottom: 8,
//     marginRight: 8,
//     // width: 125,//180,
//     // height: 120,
//     maxWidth: "100%",
//     maxHeight:"100%",
//     padding: 4,
//     boxSizing: 'border-box'
// };

// const dropzone = {
//     width: "80%",
//     height: "30%",
//     border: "1px black",
//     borderStyle: 'inset'
// }

// const thumbInner = {
//     display: 'flex',
//     minWidth: 0,
//     overflow: 'hidden'
// };

// const img = {
//     display: 'block',
//     width: 'auto',
//     height: '100%'
// };

// function Previews(/*props*/) {
//     //const { formData, setFormData } = useContext(ReferenceDataContext);
//     let file;
//     const [files, setFiles] = useState([]);
//     const { getRootProps, getInputProps } = useDropzone({
//         accept: 'image/*',
//         onDrop: acceptedFiles => {
//             setFiles(acceptedFiles.map(file => Object.assign(file, {
//                 preview: URL.createObjectURL(file)
//             })));

//             for (let i = 0; i < acceptedFiles.length; i++) {
//                 // formData.append('image_BLOB', acceptedFiles[i]);
//                 //props.candi.saveCandiImg(acceptedFiles[i])
//                 dispatch(candi.actions.saveCandiImg(acceptedFiles[i]))
//                 file = acceptedFiles[i]
//                 //formData.append('image_BLOB', acceptedFiles[i]); //.set()
//             } 
//         }
//     });

//     const thumbs = files.map(file => (
//         <div style={thumb} key={file.name}>
//             <div style={thumbInner}>
//                 <img
//                     src={file.preview}
//                     style={img}
//                 />
//             </div>
//         </div>
//     ));

//     useEffect(() => () => {
//         // Make sure to revoke the data uris to avoid memory leaks
//         files.forEach(file => URL.revokeObjectURL(file.preview));
//     }, [files]);

//     // const ref = React.createRef();
//     // useEffect(() => {
// 	// 	async function fetchData() {
// 	// 		const response = await axios.get(
// 	// 			'http://localhost:3001/candidates',
// 	// 		);
// 	// 		setFiles(response.data);
// 	// 	}
// 	// 	fetchData();
// 	// }, []);

//     return (
//         <>
//             {
//                 files.length !== 0 ? (
//                     <div style={thumbsContainer}>
//                         {thumbs}
//                     </div>
//                 ) :

//                 <div {...getRootProps()} style={dropzone} >
//                     <input {...getInputProps()} />
//                     <p><center>Upload</center></p>
//                 </div>
//             }
//         </>
//     );
// }
// // const stateToProps = state => {
// //     return {
// //       //user: state
// //       candi: state.candi
// //     };
// // }
  
// // export default connect(
// //     stateToProps,
// //     candi.actions
// // )(Previews)

// export default Previews