import React/*, { useState }*/ from 'react'
import { useHistory } from 'react-router-dom'
import axios from 'axios'
import { Field, Form, Formik } from "formik"
import { makeStyles } from '@material-ui/core/styles'
import { TextField, Grid, /*Paper,*/ Button } from '@material-ui/core'
import { Portlet, PortletBody, /*PortletFooter,*/ PortletHeader } from '../../../partials/content/Portlet'
import PhotoUpload from './photoUpload'
import { connect } from "react-redux"
import * as auth from '../../../store/ducks/auth.duck'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },

    textField: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
    },

    paper: {
        height: '450px',
    },

    spacing: {
        marginLeft: theme.spacing(32),
    },


    button: {
        background: '#0C2C53',
        "&:hover": {
            //you want this to be the same as the backgroundColor above
            backgroundColor: "#0C2C53"
        },
        width: '185px',
        height: '50px',
    },
    label: {
        textTransform: 'capitalize',
        color: 'white'
    },

}));

const useStyles1 = makeStyles(theme => ({
    root: {
        background: '#0C2C53',
        "&:hover": {
            //you want this to be the same as the backgroundColor above
            backgroundColor: "#0C2C53"
        },
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
    },
    label: {
        textTransform: 'capitalize',
        color: 'white'
    },
}));

const CustomTextField = ({ field, form, ...props }) => <TextField {...props} />

const formData = new FormData();

function OutlinedTextFields(props) {
    const history = useHistory();
    const classes = useStyles();
    const classes1 = useStyles1()
    const ref = React.createRef();
    
    return (
       <Portlet>
           <PortletHeader title="Candidate Create"></PortletHeader>
            <PortletBody>
                <Formik
                    //initialValues={currentValues}
                    initialValues = {{
                        candidates_name: "",
                        image_BLOB: "",
                        election_name: "",
                        date_of_birth: "",
                        race: "",
                        religion: "",
                        NRC_NO: "",
                        education: "",
                        occupation: "",
                        permanent_address: "",
                        why_candidate: ""
                    }}

                    onSubmit={(values, { setSubmitting }) => {
                        setTimeout(() => {
                            console.log("Entered values in candidate create form: ", values)
                            //console.log("CurrentValues.id in handleConfirm, ", currentValues.id)
                            // console.log("Values: ", values)
                            for (const [key, value] of Object.entries(values)) {
                                // if (key === "image_BLOB") { // check it is file
                                //     value = ref.current.files[0]; // assign selected file to value
                                // } 
                                formData.append(key, value)
                            }
                            console.log('Added FormData inside handleConfirm is: ')
                            formData.append('created_user', props.authData.user.fullname)
                            // for (var value of formData.values()) {
                            //     console.log(value); 
                            // }
                            for(var pair of formData.entries()) {
                                console.log(pair[0]+ ': '+ pair[1])
                            }
                            
                            console.log("Before axios post!")
                            async function postData() {
                                console.log("Before axios post. Inside postData function!")
                          
                                await axios.post(
                                    "http://localhost:3001/candidates",
                                    formData,
                                    { headers: { 'Content-Type': 'multipart/form-data' } }
                                )
                                .then(response => {
                                    console.log("Response Data inside axios post is: ", response.data)
                                })
                                .catch(error => console.log("Error is!: ", error));
                            }
                            postData();
                            console.log("After axios post!")
                            //alert("Add Successful!")
                            setSubmitting(false);
                            history.push('/party')
                        }, 1000); // 400
                    }}

                    // onChange={(e) => {
                    //     e.preventDefault();
                    //     let name = e.target.name;
                    //     let dataValue = "";
                    //     dataValue = e.target.value;

                    //     // if (name === "image_BLOB") { // check it is file
                    //     //     dataValue = ref.current.files[0]; // assign selected file to value
                    //     // } 
                    //     // else {
                    //     //     dataValue = e.target.value;
                    //     // }

                    //     setCurrentValues({
                    //         ...currentValues,
                    //         [name]: dataValue,
                    //     });
                    // }}
                >
                    {({ handleSubmit, handleChange, handleBlur, isSubmitting, values }) => (
                        <Form>
                            <div className={classes.root}>
                                <Grid container spacing={3}>
                                    <Grid item xs={2}>
                                        {/*<FileUpload addFile={this.addFile} files={this.state.files} /> */}
                                        {/* <Field
                                                id="candidates_name"
                                                name="photo"
                                                label="Upload Photo"
                                                component={CustomTextField}
                                                // style={{ margin: 8 }}
                                                fullWidth
                                                type = "file"
                                                margin="normal"
                                                variant="outlined"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                className={classes.textField}
                                            /> */}
                                            {/* <PhotoUpload/> */}
                                            {/* <PhotoUpload ref={ref} name="image_BLOB" onChange={handleChange}/> */}
                                            <PhotoUpload ref={ref} name="image_BLOB" onChange={Formik.handleChange}/>
                                    </Grid>
                                    <Grid item xs={5}>
                                        <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
                                            <Field
                                                id="candidates_name"
                                                name="candidates_name"
                                                label="Candidate Name"
                                                component={CustomTextField}
                                                // style={{ margin: 8 }}
                                                fullWidth
                                                margin="normal"
                                                variant="outlined"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                className={classes.textField}
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                //onChange={handleChange}
                                            />
                                            <Field
                                                id="date_of_birth"
                                                name="date_of_birth"
                                                label="Date Of Birth"
                                                component={CustomTextField}
                                                // style={{ margin: 8 }}
                                                fullWidth
                                                type="text"
                                                margin="normal"
                                                variant="outlined"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                className={classes.textField}
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                //onChange={handleChange}
                                            />
                                            <Field
                                                id="religion"
                                                name="religion"
                                                label="Religion"
                                                component={CustomTextField}
                                                fullWidth
                                                margin="normal"
                                                variant="outlined"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                className={classes.textField}
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                //onChange={handleChange}
                                            />
                                            <Field
                                                id="education"
                                                name="education"
                                                label="Education"
                                                component={CustomTextField}
                                                fullWidth
                                                margin="normal"
                                                variant="outlined"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                className={classes.textField}
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                            />
                                            <Field
                                                id="permanent_address"
                                                name="permanent_address"
                                                label="Permanent Address"
                                                component={CustomTextField}
                                                fullWidth
                                                margin="normal"
                                                variant="outlined"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                multiline
                                                className={classes.textField}
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                            />
                                        </div>
                                    </Grid>
                                    <Grid item xs={5}>
                                        <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
                                            <Field
                                                id="election_name"
                                                name="election_name"
                                                label="Election Name"
                                                component={CustomTextField}
                                                // style={{ margin: 8 }}
                                                fullWidth
                                                margin="normal"
                                                variant="outlined"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                className={classes.textField}
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                            />
                                            <Field
                                                id="race"
                                                name="race"
                                                label="Race"
                                                component={CustomTextField}
                                                fullWidth
                                                margin="normal"
                                                variant="outlined"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                className={classes.textField}
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                            />
                                            <Field
                                                id="NRC_NO"
                                                name="NRC_NO"
                                                label="NRC No"
                                                component={CustomTextField}
                                                fullWidth
                                                margin="normal"
                                                variant="outlined"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                className={classes.textField}
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                            />
                                            <Field
                                                id="occupation"
                                                name="occupation"
                                                label="Occupation"
                                                component={CustomTextField}
                                                fullWidth
                                                margin="normal"
                                                variant="outlined"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                className={classes.textField}
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                            />
                                            <Field
                                                id="why_candidate"
                                                name="why_candidate"
                                                label="Why would you like to be a candidate?"
                                                component={CustomTextField}
                                                fullWidth
                                                margin="normal"
                                                variant="outlined"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                multiline
                                                className={classes.textField}
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                            />
                                        </div>
                                    </Grid>
                                </Grid>
                                <Grid container spacing={3}>
                                    <Grid item xs={2}>
                                        <> </>
                                    </Grid>
                                    <Grid item xs={5} >
                                        <Button
                                            variant="contained"
                                            // type='submit'
                                            classes={{
                                                root: classes1.root, // class name, e.g. `classes-nesting-root-x`
                                                label: classes1.label, // class name, e.g. `classes-nesting-label-x`
                                            }}
                                            onClick={() => history.push('/candidate')}
                                        >
                                            Cancel
                                        </Button>
                                    </Grid>
                                    <Grid item xs={5}>
                                        <Button  
                                            variant="contained"
                                            type="submit" 
                                            disabled={isSubmitting}
                                            classes={{
                                                root: classes1.root, // class name, e.g. `classes-nesting-root-x`
                                                label: classes1.label, // class name, e.g. `classes-nesting-label-x`
                                            }}
                                            onChange={Formik.handleSubmit}
                                        >
                                            Confirm
                                        </Button>
                                    </Grid>
                                </Grid>
                            </div>
                            {/* <Button > Button</Button> */}
                            {/* <div className={classes.spacing} style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', marginTop: '20px' }}>
                            </div> */}
                        </Form>
                    )}
                </Formik>
            </PortletBody>
        </Portlet >
    );
}

// export {OutlinedTextFields as default, formData}

const stateToProps = state => {
  return {
    //user: state
    authData: state.auth
  };
}

export default connect(
  stateToProps,
  auth.actions
)(OutlinedTextFields)

export {formData}