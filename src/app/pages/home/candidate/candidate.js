
import React, { useState, useEffect } from 'react'
import { connect } from "react-redux"
import { useHistory } from "react-router-dom"
import axios from 'axios'
import MaterialTable from 'material-table'
import * as auth from '../../../store/ducks/auth.duck'

function Candidate_Table(props) {
    const history = useHistory()

    const [columns, setColumns] = useState([
        { title: 'Candidate ID', field: 'id', editable: 'never' },
        { title: 'Candidate Name', field: 'candidates_name', validate: rowData => rowData.candidates_name === '' ? { isValid: false, helperText: 'Required field' } : true },
        { title: 'Election Name', field: 'election_name', validate: rowData => rowData.election_name === '' ? { isValid: false, helperText: 'Required field' } : true },
        { title: 'Date Of Birth', field: 'date_of_birth', validate: rowData => rowData.date_of_birth === '' ? { isValid: false, helperText: 'Required field' } : true },
        { title: 'Race', field: 'race', validate: rowData => rowData.race === '' ? { isValid: false, helperText: 'Required field' } : true },
        { title: 'Religion', field: 'religion', validate: rowData => rowData.religion === '' ? { isValid: false, helperText: 'Required field' } : true },
        { title: 'Nric No', field: 'NRC_NO', validate: rowData => rowData.NRC_NO === '' ? { isValid: false, helperText: 'Required field' } : true },
        { title: 'Education', field: 'education', validate: rowData => rowData.education === '' ? { isValid: false, helperText: 'Required field' } : true },
        { title: 'Occupation', field: 'occupation', validate: rowData => rowData.occupation === '' ? { isValid: false, helperText: 'Required field' } : true },
        { title: 'Permanent Address', field: 'permanent_address', validate: rowData => rowData.permanent_address === '' ? { isValid: false, helperText: 'Required field' } : true }
    ]);

    const [data, setData] = useState([]);
    useEffect(() => {
		async function fetchData() {
            await axios.get("http://localhost:3001/candidates")
            .then(response => {
                console.log("Response Data inside axios get is: : (response.data)", response.data)

                // Removing the timezone behind date
                // E.g. 1995-06-18T17:30:00.000Z => 1995-06-18
                let responseData = response.data
                let pattern = /\d{4}-\d{2}-\d{2}/
                for (let i = 0; i < responseData.length; i++) {
                    responseData[i].date_of_birth = responseData[i].date_of_birth.match(pattern)
                }
                console.log("ResponseData after editing the date: ", responseData)
                setData(responseData);
                //setData(response.data);
            })
            .catch(error => console.log("Error is!: ", error));
        }
        fetchData();
    }, []);

    return (
        <MaterialTable
            title="Candidate Preview"
            columns={columns}
            fontFamily="Poppins"
            data={data}

            options={{
                paging:false,
                sorting:false,
                headerStyle:{fontWeight:'bold', fontSize:16, color:'#000', fontFamily:'Poppins'},
                rowStyle:{fontWeight:'normal', fontSize:16, color:'#505050', fontFamily:'Poppins'},
                actionsCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
                filterCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'}
            }}
              
            detailPanel={[
                {
                    tooltip: 'Show More',
                    render: rowData => {
                        return (
                            <div style = {{display : 'flex'}}>
                                {/* {console.log("image_BLOB inside: ", data.image_BLOB)} */}
                                {console.log("image_BLOB inside: ", rowData.image_BLOB)}
                                {/* <img src="https://fakeimg.pl/250x100/" /> */}
                                {/* <img alt="image_BLOB" src={`http://localhost:3001/candidates/${data.image_BLOB}`}/> */}
                                <img alt="image_BLOB" src={`http://localhost:3001/${rowData.image_BLOB}`} width="150" height="170" />
                                {/* <h4 style = {{margin : '50px 30px 30px 30px'}}>You just have to put your image size after our URL. Only the first parameter is mandatory. </h4> */}
                                {console.log("Row Data: ", rowData)}
                                <div>
                                    {/* <div style = {{ float: 'left', width: '50%', padding: '10px', height: '50px'}}> */}
                                    {/* <div style = {{ float: 'left', width: '40%', paddingTop: '20px', paddingLeft: '40px', paddingRight: '10px', paddingBottom: '10px', height: '50px'}}> */}
                                    <div style = {{ float: 'left', width: '40%', paddingTop: '20px', paddingLeft: '30px', paddingRight: '10px', paddingBottom: '10px', height: '50px'}}>
                                        <p><h4> Why do you want to be a candidate? </h4></p>
                                    </div>
                                    {/* <div style = {{ float: 'left', width: '50%', padding: '10px', height: '50px', textAlign: 'justify'}}> */}
                                    <div style = {{ float: 'left', width: '60%', paddingTop: '10px', paddingLeft: '10px', paddingRight: '40px', paddingBottom: '10px', height: '50px', textAlign: 'justify'}}>
                                        <p><h5> {rowData.why_candidate} </h5></p>
                                    </div>
                                </div>
                            </div>
                        )
                    },
                },
            ]}

            actions={[
                {
                    icon: 'add',
                    tooltip: 'Add',
                    isFreeAction: true,
                    onClick: (event) => history.push('/candidate/create')
                }
            ]}

            editable={{
                // onRowAdd: newData =>
                //     new Promise((resolve, reject) => {
                //         setTimeout(() => {
                //         axios.post(
                //             'http://localhost:3001/candidates', 
                //             newData,
                //             { headers: { 'Content-Type': 'application/json' } }
                //         )
                //         .then(response => {
                //             setData([...data, { id: response.data[0], ...newData }])
                //         })
                //         .catch(error => console.log(error))

                //         resolve();
                //         }, 1000)
                //     }),

                onRowUpdate: (newData, oldData) =>
                    new Promise((resolve, reject) => {
                        setTimeout(() => {
                            const index = oldData.id;
                            console.log('Index is: ', index)
                            console.log(`http://localhost:3001/candidates/${index}`)
                            // console.log('Old candidates code is: ', oldData.candidates_code)
                            // console.log('New candidates code is: ', newData.candidates_code)

                            // const formData = new FormData();
                            // for (const [key, value] of Object.entries(newData)) {
                            //     // if (key === "image_BLOB") { // check it is file
                            //     //     value = ref.current.files[0]; // assign selected file to value
                            //     // } 
                            //     formData.append(key, value)
                            // }
                            // formData.append('updated_user', props.authData.user.fullname)
                            // console.log("FormData: ")
                            // for(var pair of formData.entries()) {
                            //     console.log(pair[0]+ ': '+ pair[1])
                            // }

                            // axios.put(
                            //     `http://localhost:3001/candidates/${index}`,
                            //     formData,
                            //     { headers: { 'Content-Type': 'multipart/form-data' } }
                            // )
                            // .then(response => { 
                            //     console.log(response)
                            //     console.log("Response data id: ", response.data.id)
                            //     setData(data.map(item => item.id === response.data.id ? newData : item));
                            // })
                            // .catch(error => console.log(error))

                            // Original
                            newData = {...newData, updated_user: props.authData.user.fullname}
                            console.log('NewData: ', newData)
                            axios.put(`http://localhost:3001/candidates/${index}`, newData, { headers: { 'Content-Type': 'application/json' }} )
                            .then(response => {  // ho bak ka ny response pyn lr yin loke hmr
                                console.log(response)
                                console.log("Response data id: ", response.data.id)
                                setData(data.map(item => item.id === response.data.id ? newData : item));
                            })
                            .catch(error => {
                                console.log("In anxios catch (Candidate)")
                                console.log(error)
                            })
                            resolve();
                    }, 1000)
                }),

                onRowDelete: oldData =>
                    new Promise((resolve, reject) => {
                        setTimeout(() => {
                            const index = oldData.id;
                            console.log("onRowDelete, id: ", index)
                            
                            axios.delete(
                                `http://localhost:3001/candidates/${index}`,
                                { headers: { 'Content-Type': 'application/json' } }
                            )
                            .then(response => {
                                console.log(response)
                                console.log("Response data id: ", response.data.id)
                                setData(data.filter(item => item.id !== index));
                            })                                 
                            .catch(error => console.log("Axios delete Error is!: ", error));
                            // setData(data.filter(item => item.id !== index));
                            resolve()
                        }, 1000)
                    })
            }}
        />
    )
}

// export default Candidate_Table

const stateToProps = state => {
    return {
        //user: state
        authData: state.auth
    };
}
    
export default connect(
    stateToProps,
    auth.actions
)(Candidate_Table)