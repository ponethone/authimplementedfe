import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Candidate from './candidate'
import Create from './create'

const Index = () => {
  return (
    <Switch>
      {
        <Redirect exact from="/candidate" to="/candidate/list" />
      }
      <Route path="/candidate/list" component={Candidate} />
      <Route path="/candidate/create" component={Create} />
    </Switch>
  );
};

export default Index;



