import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import District from './district'

const Index = () => {
  return (

    <Switch>
      {
        <Redirect exact from="/district" to="/district/list" />
        // <Redirect exact from="/district" to="/district" />
      }
      <Route path="/district/list" component={District} />
      {/* <Route path="/district" component={District} /> */}
     
    </Switch>
  );
};

export default Index;



