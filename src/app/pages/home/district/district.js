// Mine
import React, { useState, useEffect } from 'react'
import { connect } from "react-redux"
import axios from 'axios'
import MaterialTable from 'material-table'
import * as auth from '../../../store/ducks/auth.duck'

function District_Table(props) {
  const [columns, setColumns] = useState([
    { title: 'District ID',   field: 'id', align:'center', editable: 'never' },
    { title: 'District Code', field: 'district_code', align:'center', validate: rowData => rowData.district_code === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'District Name', field: 'district_name', align:'center', validate: rowData => rowData.district_name === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'State/Region Name',  field: 'state_region', align:'center', validate: rowData => rowData.state_region === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'Remark', field: 'remark', align:'center' },
  ]);

  const [dataValue, setDataValue] = useState([]);
  useEffect(() => {
		async function fetchData() {
      await axios.get("http://localhost:3001/district")
      .then(response => {
          //setData(response.data);
          setDataValue(dataValue => [...dataValue, ...response.data])
          console.log("Response Data inside axios is: (response.data)", response.data)
      })
      .catch(error => console.log("Error is!: ", error));
		}
    fetchData();
  }, []);
  console.log("After axios Response Data is: ", dataValue)

  return (
    <MaterialTable
      title="District Preview"
      fontFamily="Poppins"
      columns={columns}
      data={dataValue}
      
      options={{
        paging:false,
        headerStyle:{fontWeight:'bold', fontSize:16, color:'#000', fontFamily:'Poppins'},
        rowStyle:{fontWeight:'normal', fontSize:16, color:'#505050', fontFamily:'Poppins'},
        actionsCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        filterCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'}
      }}
      
      editable={{
        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              newData = {...newData, created_user: props.authData.user.fullname}
              axios.post(
                'http://localhost:3001/district', 
                newData,
                { headers: { 'Content-Type': 'application/json' } }
              )
              .then(response => {
                  setDataValue([...dataValue, { id: response.data[0], ...newData }])
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              console.log('Index is: ', index)
              console.log('Old district code is: ', oldData.district_code)
              console.log('New district code is: ', newData.district_code)

              newData = {...newData, updated_user: props.authData.user.fullname}
              axios.put(`http://localhost:3001/district/${index}`, newData)
              .then(response => {
                  console.log(response)
                  console.log("Response data id: ", response.data.id)
                  setDataValue(dataValue.map(item => item.id === response.data.id ? newData : item));
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              axios.delete(
                `http://localhost:3001/district/${index}`,
              )
              setDataValue(dataValue.filter(item => item.id !== index));
              resolve()
            }, 1000)
          })
      }}
    />
  )
}

//export default District_Table;

const stateToProps = state => {
  return {
    //user: state
    authData: state.auth
  };
}

export default connect(
  stateToProps,
  auth.actions
)(District_Table)