import React, { useState, useEffect, forwardRef } from 'react'
import { connect } from "react-redux"
import axios from 'axios'
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
import * as auth from '../../../store/ducks/auth.duck'

function Voterlist_Table(props) {
    const tableIcons = {
        Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
        Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
        Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
        DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
        Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
        Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
        SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
        ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
        ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    const [columns, setColumns] = useState([
      { title: 'Voter ID', field: 'id', align: 'center', editable: 'never' },
      { title: 'Voter Name', field: 'voter_name', validate: rowData => rowData.voter_name === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Election Name', field: 'election_name', validate: rowData => rowData.election_name === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'NRC No', field: 'NRC_NO', validate: rowData => rowData.NRC_NO === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Date of Birth', field: 'date_of_birth', validate: rowData => rowData.date_of_birth === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Race', field: 'race', validate: rowData => rowData.race === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Religion', field: 'religion', validate: rowData => rowData.religion === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Permanent Address', field: 'permanent_address', validate: rowData => rowData.permanent_address === '' ? { isValid: false, helperText: 'Required field' } : true }
    ]);
    // const [selectedRow, setSelectedRow] = useState(null);
  
    const [data, setData] = useState([]);
    useEffect(() => {
      async function fetchData() {
        await axios.get("http://localhost:3001/voterlist")
        .then(response => {
            console.log("Response Data inside axios get is: : (response.data)", response.data)

            // Removing the timezone behind date
            // E.g. 1995-06-18T17:30:00.000Z => 1995-06-18
            let responseData = response.data
            let pattern = /\d{4}-\d{2}-\d{2}/
            for (let i = 0; i < responseData.length; i++) {
                responseData[i].date_of_birth = responseData[i].date_of_birth.match(pattern)
            }
            console.log("ResponseData after editing the date: ", responseData)
            setData(responseData);

            console.log("Response Data inside axios is: ", response.data)
        })
        .catch(error => console.log("Error is!: ", error));
      }
      fetchData();
    }, []);
    console.log("After axios Response Data is: ", data)
    
    return (
      <MaterialTable
        icons={tableIcons}
        title="Voters Preview"
        columns={columns}
        data={data}

        options={{
          rowStyle: {
            backgroundColor: '#EEE',
          }
        }}  

        editable={{
          onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              newData = {...newData, created_user: props.authData.user.fullname}
              axios.post(
                'http://localhost:3001/voterlist', 
                newData,
                { headers: { 'Content-Type': 'application/json' } }
              )
              .then(response => {
                  setData([...data, { id: response.data[0], ...newData }])
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              console.log('Index is: ', index)
              console.log('Old voterlist code is: ', oldData.voterlist_code)
              console.log('New voterlist code is: ', newData.voterlist_code)

              newData = {...newData, updated_user: props.authData.user.fullname}
              axios.put(`http://localhost:3001/voterlist/${index}`, newData)
              .then(response => {
                  console.log(response)
                  console.log("Response data id: ", response.data.id)
                  setData(data.map(item => item.id === response.data.id ? newData : item));
              })
              .catch(error => console.log(error))
              resolve();
            }, 10000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              axios.delete(
                `http://localhost:3001/voterlist/${index}`,
              )
              setData(data.filter(item => item.id !== index));
              resolve()
            }, 1000)
          })
        }}
      />
    )
}
        
// export default Voterlist_Table

const stateToProps = state => {
  return {
    //user: state
    authData: state.auth
  };
}

export default connect(
  stateToProps,
  auth.actions
)(Voterlist_Table)