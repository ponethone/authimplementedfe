import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Voterlist from './voterlist'

const Index = () => {
  return (
    <Switch>
      {
        <Redirect exact from="/voterlist" to="/voterlist/list" />
      }
      <Route path="/voterlist/list" component={Voterlist} />
      
    </Switch>

  );
};

export default Index;