import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Village from './village'

const Index = () => {
  return (
    <Switch>
      {
        <Redirect exact from="/village" to="/village/list" />
      }
      <Route path="/village/list" component={Village} />
      
    </Switch>

  );
};

export default Index;