import React, { useState, useEffect, forwardRef } from 'react'
import { connect } from "react-redux"
import axios from 'axios'
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
import * as auth from '../../../store/ducks/auth.duck'

function Editable(props) {
  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

  const [columns, setColumns] = useState([
    { title: 'Village ID', field: 'id', align:'center', editable: 'never' },
    { title: 'Village Code', field: 'village_code', align:'center', validate: rowData => rowData.village_code === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'Village Name', field: 'village_name', align:'center', validate: rowData => rowData.village_name === '' ? {isValid:false, helperText: 'Required field' } : true },
    // { title: 'Latitude', field: 'latitude', align:'center', validate: rowData => rowData.latitude === '' ? {isValid:false, helperText: 'Required field' } : true },
    // { title: 'Longitude', field: 'longitude', align:'center', validate: rowData => rowData.longitude === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'State/Region Name', field: 'state_region', align:'center', validate: rowData => rowData.state_region === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'District Name', field: 'district_name', align:'center', validate: rowData => rowData.district_name === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'Township Name', field: 'township_name', align:'center', validate: rowData => rowData.township_name === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'Village Tract Name', field: 'village_tracts_name', align:'center', validate: rowData => rowData.village_tracts_name === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'Remark', field: 'remark', align:'center' }
  ]);

  const [data, setData] = useState([]);
  useEffect(() => {
		async function fetchData() {
      // await mock.onGet("http://localhost:3001/village").reply(200);
      await axios.get("http://localhost:3001/village")
      .then(response => {
          setData(response.data);
          console.log("Response Data inside axios is: ", response.data)
      })
      .catch(error => console.log("Error is!: ", error));
		}
    fetchData();
  }, []);
  console.log("After axios Response Data is: ", data)

  return (
    <MaterialTable
    icons={tableIcons}
      title="Village Preview" 
      columns={columns}
      data={data}

      options={ { 
        paging: false, 
        sorting : false, 
        headerStyle: {fontWeight:'bold', fontSize:16, color:'#000', fontFamily:'Poppins'},
        rowStyle: {fontWeight:'normal', fontSize:16, color:'#505050', fontFamily:'Poppins'},
        actionsStyle: {fontWeight:'bold', fontSize:12, color:'#000'},
        filterStyle: {fontWeight:'bold', fontSize:12, color:'#000'}
      }}

      editable={{
        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              newData = {...newData, created_user: props.authData.user.fullname}
              axios.post(
                'http://localhost:3001/village', 
                newData,
                { headers: { 'Content-Type': 'application/json' } }
              )
              .then(response => {
                  setData([...data, { id: response.data[0], ...newData }])
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              console.log('Index is: ', index)
              console.log('Old village code is: ', oldData.village_code)
              console.log('New village code is: ', newData.village_code)

              newData = {...newData, updated_user: props.authData.user.fullname}
              axios.put(`http://localhost:3001/village/${index}`, newData)
              .then(response => {
                  console.log(response)
                  console.log("Response data id: ", response.data.id)
                  setData(data.map(item => item.id === response.data.id ? newData : item));
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              axios.delete(
                `http://localhost:3001/village/${index}`,
              )
              setData(data.filter(item => item.id !== index));
              resolve()
            }, 1000)
          })
      }}
    />
  )
}

// export default Editable

const stateToProps = state => {
  return {
    //user: state
    authData: state.auth
  };
}

export default connect(
  stateToProps,
  auth.actions
)(Editable)