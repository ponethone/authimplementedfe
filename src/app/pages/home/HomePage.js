import React, { Suspense/*, lazy*/ } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
//import Builder from "./Builder";
//import Dashboard from "./Dashboard";
//import DocsPage from "./docs/DocsPage";
import { LayoutSplashScreen } from "../../../_metronic";
import Party from './party'
import Office from './office'
import Constituency from './constituency'
import Candidate from './candidate'
import Voterlist from './voterlist'
import State from './state'
import District from './district'
import Township from './township'
import Town from './town'
import Ward from './ward'
import Village_tracts from './village_tracts'
import Village from './village'
import NewHomePage from './newhomepage'

export default function HomePage() {
  // useEffect(() => {
  //   console.log('Home page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          // <Redirect exact from="/" to="/party" />
          <Redirect exact from="/" to="/homepage" />
        }

        <Route path="/homepage" component={NewHomePage} />
        <Route path="/party" component={Party} />
        <Route path="/office" component={Office} />
        <Route path="/constituency" component={Constituency} />
        <Route path="/candidate" component={Candidate} />
        <Route path="/voterlist" component={Voterlist} />

        <Route path="/state" component={State} />
        <Route path="/district" component={District} />
        <Route path="/township" component={Township} />
        <Route path="/town" component={Town} />
        <Route path="/ward" component={Ward} />
        <Route path="/village" component={Village} />
        <Route path="/village_tracts" component={Village_tracts} />
        <Redirect to="/error/error-v1" />
      </Switch>
    </Suspense>
  );
}