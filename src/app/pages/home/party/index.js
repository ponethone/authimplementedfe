import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Party from './party'

const Index = () => {
  return (

    <Switch>
      {
        <Redirect exact from="/party" to="/party/list" />
      }
      <Route path="/party/list" component={Party} />
      
    </Switch>

  );
};

export default Index;