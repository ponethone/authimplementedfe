import React, { useState, useEffect } from 'react'
import { connect } from "react-redux"
import axios from 'axios'
import MaterialTable from 'material-table'
import * as auth from '../../../store/ducks/auth.duck'

function Party_Table(props) {
  const [columns, setColumns] = useState([
    { title: 'Party ID', field: 'id', align: 'center', editable: 'never'},
    { title: 'Party Name', field: 'party_name', align: 'center', validate: rowData => rowData.party_name === '' ? { isValid: false, helperText: 'Required field' } : true},
    { title: 'Office ID', field: 'office_id', align: 'center', validate: rowData => rowData.office_id === '' ? { isValid: false, helperText: 'Required field' } : true},
    { title: 'Constituency Name', field: 'constituency_name', align: 'center', validate: rowData => rowData.constituency_name === '' ? { isValid: false, helperText: 'Required field' } : true},
    { title: 'Description', field: 'description', align: 'center'},
  ]);

  const [data, setData] = useState([]);
  useEffect(() => {
		async function fetchData() {
      await axios.get("http://localhost:3001/party")
      .then(response => {
          setData(response.data);
          console.log("Response.data is: ", response.data)
          console.log("After setData(response.data) inside axios.get, data is: ", data)
      })
      .catch(error => console.log("Error is!: ", error));
		}
    fetchData();
  }, []);
  console.log("After axios Response Data is: ", data)

  return (
    <MaterialTable
      title="Party Preview"
      columns={columns}
      data={data}

      options={{
        paging:false,
        headerStyle:{fontWeight: 'bold', fontSize:16, color: '#000', fontFamily: 'Poppins'},
        rowStyle:{fontWeight: 'normal', fontSize:16, color: '#505050', fontFamily: 'Poppins'},
        actionsCellStyle:{fontWeight: 'bold', fontSize:12, color: '#000'},
        filterCellStyle:{fontWeight: 'bold', fontSize:12, color: '#000'}
      }}
      
      editable={{
          onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              newData = {...newData, created_user: props.authData.user.fullname}
              axios.post(
                'http://localhost:3001/party', 
                newData,
                { headers: { 'Content-Type': 'application/json' } }
              )
              .then(response => {
                  setData([...data, { id: response.data[0], ...newData }])
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

          onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              console.log('Index is: ', index)
              console.log('Old party code is: ', oldData.party_code)
              console.log('New party code is: ', newData.party_code)

              newData = {...newData, updated_user: props.authData.user.fullname}
              console.log("newData for fullname (Party): "); console.log(newData);
              axios.put(`http://localhost:3001/party/${index}`, newData)
              .then(response => {
                  console.log(response)
                  console.log("Response data id: ", response.data.id)
                  setData(data.map(item => item.id === response.data.id ? newData : item));
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              axios.delete(
                `http://localhost:3001/party/${index}`,
              )
              setData(data.filter(item => item.id !== index));
              resolve()
            }, 1000)
          })
      }}
    />
  )
}

// export default Party_Table

const stateToProps = state => {
  return {
    //user: state
    authData: state.auth
  };
}

export default connect(
  stateToProps,
  auth.actions
)(Party_Table)