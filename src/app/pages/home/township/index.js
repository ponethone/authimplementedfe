import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Township from './township'

const Index = () => {
  return (
    <Switch>
      {
        <Redirect exact from="/township" to="/township/list" />
      }
      <Route path="/township/list" component={Township} />
    </Switch>

  );
};

export default Index;



