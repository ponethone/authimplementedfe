import React, { useState, useEffect } from 'react'
import { connect } from "react-redux"
import axios from 'axios'
import MaterialTable from 'material-table'
import * as auth from '../../../store/ducks/auth.duck'

function OfficeTable(props) {
  const [columns, setColumns] = useState([
    { title: 'Office ID', field: 'id', editable: 'never' },
    { title: 'Office Name', field: 'office_name', validate: rowData => rowData.office_name === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'Address', field: 'address', validate: rowData => rowData.address === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'State/Region Name', field: 'state_region', validate: rowData => rowData.state_region === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'District Name', field: 'district_name', validate: rowData => rowData.district_name === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'Town Name', field: 'town_name', validate: rowData => rowData.town_name === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'Township Name', field: 'township_name', validate: rowData => rowData.township_name === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'Ward Name', field: 'ward_name', validate: rowData => rowData.ward_name === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'Village Tract Name', field: 'village_tracts_name', validate: rowData => rowData.village_tracts_name === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'Village Name', field: 'village_name', validate: rowData => rowData.village_name === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'Ph No 1', field: 'ph_no1', validate: rowData => rowData.ph_no1 === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'Ph No 2', field: 'ph_no2', validate: rowData => rowData.ph_no2 === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'Email', field: 'email', validate: rowData => rowData.email === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'Building Type', field: 'building_type', validate: rowData => rowData.building_type === '' ? { isValid: false, helperText: 'Required field' } : true },
    { title: 'Own/Rental', field: 'own_rental', validate: rowData => rowData.own_rental === '' ? { isValid: false, helperText: 'Required field' } : true },
  ])
  // const [selectedRow, setSelectedRow] = useState(null);

  const [data, setData] = useState([]);
  useEffect(() => {
		async function fetchData() {
      await axios.get("http://localhost:3001/office")
      .then(response => {
          setData(response.data);
          console.log("Response Data inside axios is: ", response.data)
      })
      .catch(error => console.log("Error is!: ", error));
		}
    fetchData();
  }, []);
  console.log("After axios Response Data is: ", data)

  return (
    <MaterialTable
      title= "Office Preview"
      columns={columns}
      data={data}
      
      editable={{
        onRowAdd: newData =>
        new Promise((resolve, reject) => {
          setTimeout(() => {
            newData = {...newData, created_user: props.authData.user.fullname}
            axios.post(
              'http://localhost:3001/office', 
              newData,
              { headers: { 'Content-Type': 'application/json' } }
            )
            .then(response => {
                setData([...data, { id: response.data[0], ...newData }])
            })
            .catch(error => console.log(error))
            resolve();
          }, 1000)
        }),

      onRowUpdate: (newData, oldData) =>
        new Promise((resolve, reject) => {
          setTimeout(() => {
            const index = oldData.id;
            console.log('Index is: ', index)
            console.log('Old office code is: ', oldData.office_code)
            console.log('New office code is: ', newData.office_code)

            newData = {...newData, updated_user: props.authData.user.fullname}
            console.log("newData for fullname (Office): "); console.log(newData);
            axios.put(`http://localhost:3001/office/${index}`, newData)
            .then(response => {
                console.log(response)
                console.log("Response data id: ", response.data.id)
                setData(data.map(item => item.id === response.data.id ? newData : item));
            })
            .catch(error => console.log(error))
            resolve();
          }, 1000)
        }),

      onRowDelete: oldData =>
        new Promise((resolve, reject) => {
          setTimeout(() => {
            const index = oldData.id;
            axios.delete(
              `http://localhost:3001/office/${index}`,
            )
            setData(data.filter(item => item.id !== index));
            resolve()
          }, 1000)
        })
      }}
    />
  );    
}

// export default OfficeTable

const stateToProps = state => {
  return {
    //user: state
    authData: state.auth
  };
}

export default connect(
  stateToProps,
  auth.actions
)(OfficeTable)