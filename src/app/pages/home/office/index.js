import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Office from './office'

const Index = () => {
  return (
    <Switch>
      {
        <Redirect exact from="/office" to="/office/list" />
      }
      <Route path="/office/list" component={Office} />
      
    </Switch>

  );
};

export default Index;