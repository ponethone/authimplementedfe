import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Village_tracts from './village_tracts'

const Index = () => {
  return (
    <Switch>
      {
        <Redirect exact from="/village_tracts" to="/village_tracts/list" />
      }
      <Route path="/village_tracts/list" component={Village_tracts} />
      
    </Switch>

  );
};

export default Index;