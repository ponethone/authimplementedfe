import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import axios from 'axios';
import MaterialTable from 'material-table';
import * as auth from '../../../store/ducks/auth.duck'

function State_Table(props) {
  const [columns, setColumns] = useState([
    { title: 'State ID', field: 'id', align:'center', editable: 'never'},
    { title: 'State Code', field: 'state_code', align:'center', validate: rowData => rowData.state_code === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'State/Region Name', field: 'state_region', align:'center', validate: rowData => rowData.state_region === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'Remark', field: 'remark', align:'center' },
  ]);

  const [data, setData] = useState([]);
  useEffect(() => {
		async function fetchData() {
      await axios.get("http://localhost:3001/state")
      .then(response => {
          setData(response.data);
          console.log("Response Data inside axios is: (response data", response.data)
      })
      .catch(error => console.log("Error is!: ", error));
		}
    fetchData();
  }, []);
  console.log("After axios Response Data is: ", data)

  return (
    <MaterialTable
      title="State Preview"
      fontFamily="Poppins"
      columns={columns}
      data={data}

      options={{
        paging:false,
        sorting:false,
        headerStyle:{fontWeight:'bold', fontSize:16, color:'#000', fontFamily:'Poppins'},
        rowStyle:{fontWeight:'normal', fontSize:16, color:'#505050', fontFamily:'Poppins'},
        actionsCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        filterCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'}
      }}
      
      editable={{
        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              newData = {...newData, created_user: props.authData.user.fullname}
              // console.log('newData after:', newData)
              axios.post(
                'http://localhost:3001/state', 
                newData,
                { headers: { 'Content-Type': 'application/json' } }
              )
              .then(response => {
                  setData([...data, { id: response.data[0], ...newData }])
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              console.log('Index is: ', index)
              console.log('Old state code is: ', oldData.state_code)
              console.log('New state code is: ', newData.state_code)

              newData = {...newData, updated_user: props.authData.user.fullname}
              axios.put(`http://localhost:3001/state/${index}`, newData)
              .then(response => {
                  console.log(response)
                  console.log("Response data id: ", response.data.id)
                  setData(data.map(item => item.id === response.data.id ? newData : item));
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              axios.delete(
                `http://localhost:3001/state/${index}`,
              )
              setData(data.filter(item => item.id !== index));
              resolve()
            }, 1000)
          })
      }}
    />
  )
}

//export default State_Table;

const stateToProps = state => {
  return {
    //user: state
    authData: state.auth
  };
}

export default connect(
  stateToProps,
  auth.actions
)(State_Table)