import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import State from './state'

const Index = () => {
  return (
    <Switch>
      {
        <Redirect exact from="/state" to="/state/list" />
        // <Redirect exact from="/state" to="/state" />
      }
      <Route path="/state/list" component={State} />
      {/* <Route path="/state" component={State} /> */}
     
    </Switch>
  );
};

export default Index;