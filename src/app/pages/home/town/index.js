import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Town from './town'

const Index = () => {
  return (
    <Switch>
      {
        <Redirect exact from="/town" to="/town/list" />
      }
      <Route path="/town/list" component={Town} />
    </Switch>

  );
};

export default Index;



