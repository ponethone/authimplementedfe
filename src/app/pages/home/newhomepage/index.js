import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import NewHomePage from './newhomepage'

const Index = () => {
  return (

    <Switch>
      <Route path="/homepage" component={NewHomePage} />
    </Switch>

  );
};

export default Index;