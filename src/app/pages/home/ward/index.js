import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Ward from './ward'

const Index = () => {
  return (
    <Switch>
      {
        <Redirect exact from="/ward" to="/ward/list" />
      }
      <Route path="/ward/list" component={Ward} />
      
    </Switch>

  );
};

export default Index;