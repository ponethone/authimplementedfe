import React, { useState, useEffect } from 'react'
import { connect } from "react-redux"
import axios from 'axios'
import MaterialTable from 'material-table'
import * as auth from '../../../store/ducks/auth.duck'

function Ward_Table(props) {
  const [columns, setColumns] = useState([
    { title: 'Ward ID', field: 'id', align:'center', editable: 'never' },
    { title: 'Ward Code', field: 'ward_code', align:'center', validate: rowData => rowData.ward_code === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'Ward Name', field: 'ward_name', align:'center', validate: rowData => rowData.ward_name === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'State/Region Name', field: 'state_region', align:'center', validate: rowData => rowData.state_region === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'District Name', field: 'district_name', align:'center', validate: rowData => rowData.district_name === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'Township Name', field: 'township_name', align:'center', validate: rowData => rowData.township_name === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'Town Name', field: 'town_name', align:'center', validate: rowData => rowData.town_name === '' ? {isValid:false, helperText: 'Required field' } : true },
    { title: 'Remark', field: 'remark', align:'center' }
  ]);
  // const [selectedRow, setSelectedRow] = useState(null);

  const [data, setData] = useState([]);
  useEffect(() => {
		async function fetchData() {
      await axios.get("http://localhost:3001/ward")
      .then(response => {
          setData(response.data);
          console.log("Response Data inside axios is: ", response.data)
      })
      .catch(error => console.log("Error is!: ", error));
		}
    fetchData();
  }, []);
  console.log("After axios Response Data is: ", data)

  return (
    <MaterialTable
      title="Ward Preview"
      fontFamily="Poppins"
      columns={columns}
      data={data}

      options={{
        paging:false,
        headerStyle:{fontWeight:'bold', fontSize:16, color:'#000', fontFamily:'Poppins'},
        rowStyle:{fontWeight:'normal', fontSize:16, color:'#505050', fontFamily:'Poppins'},
        actionsCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        filterCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'}
      }}
      
      editable={{
        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              newData = {...newData, created_user: props.authData.user.fullname}
              axios.post(
                'http://localhost:3001/ward', 
                newData,
                { headers: { 'Content-Type': 'application/json' } }
              )
              .then(response => {
                  setData([...data, { id: response.data[0], ...newData }])
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              console.log('Index is: ', index)
              console.log('Old ward code is: ', oldData.ward_code)
              console.log('New ward code is: ', newData.ward_code)

              newData = {...newData, updated_user: props.authData.user.fullname}
              axios.put(`http://localhost:3001/ward/${index}`, newData)
              .then(response => {  // ho bak ka ny response pyn lr yin loke hmr
                  console.log(response)
                  console.log("Response data id: ", response.data.id)
                  setData(data.map(item => item.id === response.data.id ? newData : item));
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              axios.delete(
                `http://localhost:3001/ward/${index}`,
              )
              setData(data.filter(item => item.id !== index));
              resolve()
            }, 1000)
          })
      }}
    />
  )
}

// export default Ward_Table;

const stateToProps = state => {
  return {
    //user: state
    authData: state.auth
  };
}

export default connect(
  stateToProps,
  auth.actions
)(Ward_Table)