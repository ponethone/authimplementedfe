import React, { useState, useEffect/*, forwardRef*/ } from 'react'
import axios from 'axios'
import MaterialTable from 'material-table'
import { connect } from "react-redux"
import * as auth from '../../../store/ducks/auth.duck'

function Constituency_Table(props) {    
    const [columns, setColumns] = useState([
      { title: 'Constituency ID', field: 'id', editable: 'never' },
      { title: 'Constituency Name', field: 'constituency_name', validate: rowData => rowData.constituency_name === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Election Name', field: 'election_name', validate: rowData => rowData.election_name === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Code 1', field: 'code1', validate: rowData => rowData.code1 === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Code 2', field: 'code2', validate: rowData => rowData.code2 === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Village Name', field: 'village_name', validate: rowData => rowData.village_name === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Village Tract Name', field: 'village_tracts_name', validate: rowData => rowData.village_tracts_name === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Ward Name', field: 'ward_name', validate: rowData => rowData.ward_name === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Town Name', field: 'town_name', validate: rowData => rowData.town_name === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Township Name', field: 'township_name', validate: rowData => rowData.township_name === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'District Name', field: 'district_name', validate: rowData => rowData.district_name === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'State/Region Name', field: 'state_region', validate: rowData => rowData.state_region === '' ? { isValid: false, helperText: 'Required field' } : true },
      { title: 'Party Name', field: 'party_name', validate: rowData => rowData.party_name === '' ? { isValid: false, helperText: 'Required field' } : true }
    ]);
    // const [selectedRow, setSelectedRow] = useState(null);
  
    const [data, setData] = useState([]);
    useEffect(() => {
      async function fetchData() {
        await axios.get("http://localhost:3001/constituency")
        .then(response => {
            setData(response.data);
            console.log("Response Data inside axios is: ", response.data)
        })
        .catch(error => console.log("Error is!: ", error));
      }
      fetchData();
    }, []);
    console.log("After axios Response Data is: ", data)
    
    return (
      <MaterialTable
        title="Constituency Preview"
        columns={columns}
        data={data}

        options={{
          rowStyle: {
            backgroundColor: '#EEE',
          },
          search: true
        }}  

        editable={{
          onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              newData = {...newData, created_user: props.authData.user.fullname}
              axios.post(
                'http://localhost:3001/constituency', 
                newData,
                { headers: { 'Content-Type': 'application/json' } }
              )
              .then(response => {
                  setData([...data, { id: response.data[0], ...newData }])
              })
              .catch(error => console.log(error))
              resolve();
            }, 1000)
          }),

        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              console.log('Index is: ', index)
              console.log('Old constituency code is: ', oldData.constituency_code)
              console.log('New constituency code is: ', newData.constituency_code)

              newData = {...newData, updated_user: props.authData.user.fullname}
              console.log('newData', newData)
              axios.put(`http://localhost:3001/constituency/${index}`, newData)
              .then(response => {
                  console.log(response)
                  console.log("Response data id: ", response.data.id)
                  setData(data.map(item => item.id === response.data.id ? newData : item));
              })
              .catch(error => console.log(error))
              resolve();
            }, 10000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const index = oldData.id;
              axios.delete(
                `http://localhost:3001/constituency/${index}`,
              )
              setData(data.filter(item => item.id !== index));
              resolve()
            }, 1000)
          })
        }}
      />
    )
}
        
// export default Constituency_Table

const stateToProps = state => {
  return {
    //user: state
    authData: state.auth
  };
}

export default connect(
  stateToProps,
  auth.actions
)(Constituency_Table)