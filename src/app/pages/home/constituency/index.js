import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Constituency from './constituency'

const Index = () => {
  return (
    <Switch>
      {
        <Redirect exact from="/constituency" to="/constituency/list" />
      }
      <Route path="/constituency/list" component={Constituency} />
      
    </Switch>

  );
};

export default Index;